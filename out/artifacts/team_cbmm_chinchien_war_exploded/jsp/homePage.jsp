<%@ page import="DAOs.User" %><%--
  Description: a home page for each user
  ----------------------------------------------------------
  Version  |   Date        |   Created by          |   Description
  v1       |   22/05/2018  |   Chinchien & Massie  |
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>HomePage</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <%--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>
    <link rel="stylesheet" type="text/css" href="<c:url value='../css/page.css' />"/>

    <style>

        .commentArticleId{
            display: none;
        }

        #registerModal {
            width: 800px;
        }

        #editTextArea {
            display: none;
        }

        .save {
            display: none;
        }



        .comment {
            /*for styling comments in the table*/
            width: 250px;
            height: 35px;
            background-color: forestgreen;
            margin: 5px;
            border-radius: 5px;
            text-align: left;
            padding: 10px;
            margin-left: 10px;
            display: none;
        }


        .newcomment {
            /*for styling comments in the table*/
            width: 250px;
            height: 35px;
            background-color: forestgreen;
            margin: 5px;
            border-radius: 5px;
            text-align: left;
            padding: 10px;
            margin-left: 10px;
            display: none;
        }

        #articleModal {
            padding: 20px;
        }

        .smallCommentBtn {
            width: 100px;
            height: 35px;
            font-size: 10px;
            display: inline;
        }

        .tinyComment {
            transform: translate(25px, 0px);
            width: 85px;
            height: 35px;
            background-color: #3B5998;
            border-radius: 20px
        }

        .tinyCommentBtn {
            transform: translate(10px, 0px);
            width: 75px;
            height: 25px;
        }

        .smallText {
            display: inline;
            height: 35px;
        }

        .tinyText {
            width: 75px;
            height: 25px;
            display: inline;
        }

        #commentArea {
            padding: 10px;
            margin: 10px;
        }

        #editTitle {
            display: none;
        }

        #saveButton {
            display: none;
        }
    </style>
    <%--summernote--%>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
</head>
<body>
<header>
    <%@ include file="navbar.jsp" %>
</header>
<div class="container">
    <div class="row">
        <div class="col-md-4 informationColumn">
            <div class="col-md-12 contentCard">
                <c:choose>
                    <c:when test="${ownerInfo.avatar==null}">
                        <img id="avatar" src="../image/avatar_default.png" style="width: 200px">
                    </c:when>
                    <c:otherwise>
                        <img id="avatar" src="../image/${ownerInfo.avatar}" style="width: 200px">
                    </c:otherwise>
                </c:choose>
                <div class="userInfor">
                    <div class="userInforRow">
                        <p class="userInforTitle">UserName :</p>
                        <p class="userInforDetail">${ownerInfo.username}<c:if
                                test="${ownerInfo.username == null}">none</c:if></p>
                    </div>
                    <div class="userInforRow">
                        <p class="userInforTitle">Email :</p>
                        <p class="userInforDetail">${ownerInfo.email}<c:if
                                test="${ownerInfo.email == null}">none</c:if></p>
                    </div>
                    <div class="userInforRow">
                        <p class="userInforTitle">Birthday :</p>
                        <p class="userInforDetail">${ownerInfo.dob}<c:if test="${ownerInfo.dob == null}">none</c:if></p>
                    </div>
                    <div class="userInforRow">
                        <p class="userInforTitle">Country :</p>
                        <p class="userInforDetail">${ownerInfo.country}<c:if
                                test="${ownerInfo.country == null}">none</c:if></p>
                    </div>
                    <div class="userInforRow">
                        <p class="userInforTitle">Description :</p>
                        <p class="userInforDetail" style="text-align: justify">${ownerInfo.descrp}<c:if
                                test="${ownerInfo.descrp == null}">none</c:if></p>
                    </div>
                    <br>
                    <br>
                    <div class="userInforRow">
                        <p class="userInforTitle" style="float: right; color: #2aabd2;">Edit</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 articleColumn">
            <c:choose>
                <c:when test="${fn:length(articles) gt 0}">

                    <c:forEach var="article" items="${articles}">



                        <div class="row contentCard02" id="${article.articleId}">
                            <div class="col-md-12">
                                <h5 class="card-title">${article.title}</h5>
                                <p class="author">By / <span>${article.username}</span></p>
                                    <%--only show short content with 200 chars--%>
                                <hr class="line">
                                <p class="card-text">${fn:substring(article.content, 0, 199)}</p>

                            </div>
                            <button type="button" class="btn btn-danger load" data-toggle="modal" data-target="#articleModal"
                                    data-username="${article.username}" data-content="${article.content}" data-title="${article.title}"
                                    data-id="${article.articleId}" >
                                Load Article
                            </button>

                            <c:choose>
                                <c:when test="${username == article.username}">
                                    <button type="button" id="deleteButton" class="btn btn-danger" onclick="deleteArticle(${article.articleId})"
                                            data-articleID="">Delete Article
                                    </button>
                                    <button type="button" class="btn btn-danger load" data-toggle="modal" data-target="#editModal"
                                            data-username="${article.username}" data-content="${article.content}" data-title="${article.title}"
                                            data-id="${article.articleId}" >
                                        Edit Article
                                    </button>
                                </c:when>
                                <c:otherwise>
                                    <p>hello</p>
                                </c:otherwise>
                            </c:choose>

                        </div>

                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <p>No articles!</p>
                </c:otherwise>
            </c:choose>



        </div>
    </div>
</div>

<%@include file="articleModal.jsp"%>
<%@include file="scripts.jsp"%>
<script>
    $(document).ready(function () {
        $('.summernote').summernote({
            height: 300
            // focus: true
        });
        // $('.dropdown-toggle').dropdown();
    });
</script>


</body>
</html>
