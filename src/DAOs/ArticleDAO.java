package DAOs;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ArticleDAO implements AutoCloseable {
    private final Connection conn;


    public ArticleDAO() throws IOException, SQLException {
        this.conn = HikariConnectionPool.getConnection();
    }

    //todo get what we want from the DB by different methods
    public List<Article> getAllArticles() throws SQLException, ParseException {
        List<Article> articles = new ArrayList<>();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM clin864.blog_articles")) {
            try (ResultSet r = stmt.executeQuery()) {
                while (r.next()) {
                    Article article = new Article();
                    article.setArticleId(r.getInt("article_id"));
                    article.setTitle(r.getString("title"));
                    article.setContent(r.getString("content"));
                    article.setModifiedDateAndTime(r.getString("modified_date").substring(0,10));
                    article.setStatus((r.getString("status")));
                    article.setUsername(r.getString("username"));



                    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");


                    String currentTime = sdf.format(new Date());
                    String modifyTime = (r.getString("modified_date")).substring(0,19);
                    Date currentTimeDate=sdf.parse (currentTime);

                    Date modifyTimeDate=sdf.parse (modifyTime);
                    if (modifyTimeDate.before(currentTimeDate)) {
                        articles.add(article);
                    }

                }
            } catch (SQLException e) {
                System.out.println("An Exception occured ");
            }
        }

        return articles;
    }


    public List<Article> getArticlesByName(String name) throws SQLException {
        List<Article> articles = new ArrayList<>();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM blog_articles WHERE username LIKE ?")) {

            stmt.setString(1, name);
            try (ResultSet r = stmt.executeQuery()) {
                while (r.next()) {
                    Article article = new Article();
                    article.setArticleId(r.getInt("article_id"));
                    article.setTitle(r.getString("title"));
                    article.setContent(r.getString("content"));
                    article.setModifiedDateAndTime(r.getString("modified_date").substring(0,10));
                    article.setStatus((r.getString("status")));
                    article.setUsername(r.getString("username"));
                    articles.add(article);
                }

                return articles;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return articles;
    }


    public List<Article> getArticlesByTitle(String title) throws SQLException {
        List<Article> articles = new ArrayList<>();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM blog_articles WHERE title LIKE ?")) {

            stmt.setString(1, title);
            try (ResultSet r = stmt.executeQuery()) {
                while (r.next()) {
                    Article article = new Article();
                    article.setArticleId(r.getInt("article_id"));
                    article.setTitle(r.getString("title"));
                    article.setContent(r.getString("content"));
                    article.setModifiedDateAndTime(r.getString("modified_date").substring(0,10));
                    article.setStatus((r.getString("status")));
                    article.setUsername(r.getString("username"));
                    articles.add(article);
                }

                return articles;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return articles;
    }

    public List<Article> getArticlesByDate(String modified_date) throws SQLException {
        List<Article> articles = new ArrayList<>();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM blog_articles WHERE modified_date LIKE ?")) {

            stmt.setString(1, modified_date);
            try (ResultSet r = stmt.executeQuery()) {
                while (r.next()) {
                    Article article = new Article();
                    article.setArticleId(r.getInt("article_id"));
                    article.setTitle(r.getString("title"));
                    article.setContent(r.getString("content"));
                    article.setModifiedDateAndTime(r.getString("modified_date").substring(0,10));
                    article.setStatus((r.getString("status")));
                    article.setUsername(r.getString("username"));
                    articles.add(article);
                }

                return articles;

            } catch (SQLException e) {
                System.out.println("Exception occured");
            }
        }
        return articles;
    }


    public void createNewArticle(Article article) {

        try(  PreparedStatement stmt = conn.prepareStatement("INSERT INTO clin864.blog_articles (title, content, username, modified_date)  VALUES (?,?,?,?)");) {

            {

                stmt.setString(1, article.getTitle());
                stmt.setString(2, article.getContent());
                stmt.setString(3, article.getUsername());
                stmt.setString(4, article.getModifiedDateAndTime());
                stmt.executeUpdate();



            }

        } catch (SQLException e) {
            System.out.println("Exception occured");
        }
    }


    public void modifyArticle(HttpSession session) {

        String name=(String)session.getAttribute("titleToGive");
        String newContent=(String)session.getAttribute("textToGive");
        int id=Integer.parseInt((String)session.getAttribute("iDToGive"))  ;


//

        try (PreparedStatement stmt = conn.prepareStatement(" UPDATE clin864.blog_articles SET content=?,title=? WHERE article_id=?")) {

            stmt.setString(1, newContent);
            stmt.setString(2, name);
            stmt.setInt(3, id);

            stmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println("Exception occured");
        }




    }



    public void deleteArticle(String name) throws SQLException {


        int id=Integer.parseInt(name);
        System.out.println(name);
        try (PreparedStatement stmt = conn.prepareStatement("DELETE FROM clin864.blog_articles WHERE blog_articles.article_id LIKE ?")) {
            {
               stmt.setInt(1,id);
               stmt.executeUpdate();

            }

        } catch (SQLException e) {
            System.out.println("Exception occured");
        }


    }


    @Override
    public void close() throws Exception {
        this.conn.close();
    }
}
