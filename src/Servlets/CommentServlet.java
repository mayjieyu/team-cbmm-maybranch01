package Servlets;

import DAOs.Article;
import DAOs.ArticleDAO;
import DAOs.Comment;
import DAOs.CommentDAO;
import com.google.gson.Gson;
import org.owasp.encoder.Encode;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class CommentServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

//        System.out.println("I got to doGet!");
//
//       req.getSession().setAttribute("articleID",req.getParameter("articleID"));
//        System.out.println(req.getSession().getAttribute("articleID"));
//        displayCommentsList(req,resp);



    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        Cookie[] cookies = req.getCookies();

        for (int i = 0; i < cookies.length; i++) {
            String name = cookies[i].getName();
            String value = cookies[i].getValue();



            if (value.equals("delete")) {
//                req.getSession().setAttribute("newComment",req.getParameter("commentTextArea"));
//                this won't work, you have to find the particular area of the comment you want to delete.

                       req.getSession().setAttribute("commentID", Encode.forJava(req.getParameter("commentID")  ) );
               


                        deleteComment(req, resp);
                cookies[i].setMaxAge(0);

            }
            if (value.equals("create")){

//                We already have the username in the session..
//                We already have the article id in the form


                req.getSession().setAttribute("commentText",Encode.forJava(req.getParameter("text"))  );
                req.getSession().setAttribute("articleID", Encode.forJava(req.getParameter("articleID"))    );

// TODO: 29/05/18 delete these cookies after you use them! 

                makeNewComment(req,resp);
                cookies[i].setMaxAge(0);

            }
        }
    }

//    private void displayCommentsList(HttpServletRequest request, HttpServletResponse response) {
//
//        try (CommentDAO toGetList=new CommentDAO()){
//
//            String placeholder=(String)request.getSession().getAttribute("articleID");
//            int articleID=Integer.parseInt(placeholder);
//
//
//            List<Comment> toStoreComments=toGetList.getAllCommentsByArticleId(articleID);
//
//            request.getSession().setAttribute("comments",toStoreComments);
//
//
//
////            request.getRequestDispatcher("/jsp/mainPage.jsp").forward(request, response);
//
//        }catch (Exception e){
//            System.out.println("Exception occured");
//        }
//
//
//    }


    private void makeNewComment(HttpServletRequest req, HttpServletResponse resp){

        HttpSession session=req.getSession();
        try  (CommentDAO makeNewComment=new CommentDAO()){
            Comment comment=new Comment();

            String newContent= Encode.forJava((String)session.getAttribute("commentText")) ;
            System.out.println(newContent);
            String placeHolder= Encode.forJava((String)session.getAttribute("articleID"))  ;

            int articleID=Integer.parseInt(placeHolder);


            String userName= Encode.forJava ((String)session.getAttribute("username")) ;


            comment.setArticleId(articleID);
            comment.setContent(newContent);
            comment.setUsername(userName);


            makeNewComment.createNewComment(comment);
        }catch (Exception e){
            System.out.println("Exception occured");
        }
    }






    private void deleteComment(HttpServletRequest request,HttpServletResponse response){

        //this method creates an ArticleDAO and from there, calls the delete article method to remove it from the database
        System.out.println("I got to deleteComment!");

        String commentID=(String) request.getSession().getAttribute("commentID");
        int id=Integer.parseInt(commentID);

        try (CommentDAO toControl=new CommentDAO()){

            toControl.deleteComment(id);


            //redirects to main page
            request.getRequestDispatcher("articles.jsp").forward(request,response);


        } catch (Exception e) {
            System.out.println("Exception occured");
        }


    }
}
