package Servlets;

import DAOs.User;
import DAOs.UserDAO;
import org.owasp.encoder.Encode;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class UserInfoServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String userName=(String)req.getSession().getAttribute("userName");

        try(UserDAO getInfo=new UserDAO()) {


            User toReturn=getInfo.getAllUserInfoByName(userName);
            req.getSession().setAttribute("user", toReturn);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/UpdateInfo.jsp");
            dispatcher.forward(req, resp);


        }catch (Exception e){
            System.out.println("Exception occured");
        }


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String userName=(String)req.getSession().getAttribute("userName");
        try (UserDAO getInfo=new UserDAO()){

            User toReturn=new User();



           toReturn.setFname(Encode.forJava(req.getParameter("fname")));
            toReturn.setLname(  Encode.forJava(req.getParameter("lname")));
            toReturn.setPassword(Encode.forJava(req.getParameter("password")));
            toReturn.setEmail( Encode.forJava(req.getParameter("email")));
            toReturn.setDob( Encode.forJava(req.getParameter("dob")));
            toReturn.setCountry(Encode.forJava(req.getParameter("country")));
           toReturn.setAvatar(req.getParameter("avatar"));
            toReturn.setRole( Encode.forJava(req.getParameter("role")));

            getInfo.modifyUser(toReturn);

        }catch (SQLException e){
            System.out.println(" An Exception occured");
        } catch (Exception e) {
            System.out.println(" An Exception occured");
        }


    }
}
