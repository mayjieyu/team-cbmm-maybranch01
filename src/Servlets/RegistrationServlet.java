package Servlets;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jspsmart.upload.SmartUpload;
import com.jspsmart.upload.SmartUploadException;
import DAOs.User;
import DAOs.UserDAO;
import org.owasp.encoder.Encode;

import javax.imageio.ImageIO;
import java.io.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.sql.SQLException;

public class RegistrationServlet extends HttpServlet {
    private SmartUpload su;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //initialise smartUpload object
        su = new SmartUpload();
        su.initialize(this, req, resp);
        su.setMaxFileSize(1024 * 1024 * 10);
        su.setAllowedFilesList("jpg,jpeg,bmp,png");
        try {
            su.setDeniedFilesList("exe,jsp,bat,html,,");
            su.upload();
        } catch (SQLException e) {
            System.out.println("Exception occured");
            resp.sendRedirect("/badreg,jsp");
        } catch (SmartUploadException e) {
            resp.sendRedirect("/badreg,jsp");
        }

        //create an new user
        try {
            createNewUser(req, resp);
        } catch (Exception e) {
            System.out.println("Exception occured");
            resp.sendRedirect("/badreg,jsp");
        }
    }





    public void createNewUser(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        User newUser = new User();

                String password=su.getRequest().getParameter("password");
                String hashedPassword=passwordGenerator(password);



       newUser.setUsername(Encode.forJava(su.getRequest().getParameter("username")));
        newUser.setPassword(hashedPassword);
        newUser.setFname(Encode.forJava(su.getRequest().getParameter("fname")) );
        newUser.setLname(Encode.forJava(su.getRequest().getParameter("lname")));
        newUser.setEmail(Encode.forJava(su.getRequest().getParameter("email")));
        newUser.setDob(Encode.forJava(su.getRequest().getParameter("dob")));
        newUser.setCountry(Encode.forJava(su.getRequest().getParameter("country")));
        newUser.setDescrp(Encode.forJava(su.getRequest().getParameter("descrp")));




        if (!su.getRequest().getParameter("avatar").equals("myImg")) {
            newUser.setAvatar(su.getRequest().getParameter("avatar"));
        } else {
            try {
                String ext = su.getFiles().getFile(0).getFileExt(); //file type
                String filename =Encode.forJava( su.getRequest().getParameter("username")) + "." + ext;
                if (filename != null) {
                    String pa = this.getServletContext().getRealPath("/") + "image" + File.separator + filename;
                    su.getFiles().getFile(0).saveAs(pa);
                    File folder = new File(this.getServletContext().getRealPath("/") + "image");
                    File file = new File(pa);
                    generateThumbnail(folder, file);

                    newUser.setAvatar(filename);
                } else {
                    newUser.setAvatar("avatar_default.png");
                }
            } catch (SmartUploadException e) {
                System.out.println("Exception occured");
                resp.sendRedirect("/badreg,jsp");
            }
        }

        try (UserDAO userDAO = new UserDAO()) {
            userDAO.createNewUser(newUser);
            PrintWriter out = resp.getWriter();
            out.println("<html>");
            out.println("<head>");
            out.println("<title>result</title>");
            out.println("</head>");
            out.println("<body>");
            out.print("Yor account, " + newUser.getUsername() + ", has been created.");
            out.print("<br>");
            out.print("<a href = \"login.html\">log in</a>");
            out.println("</body>");
            out.println("</html>");
        }

    }

    public static String passwordGenerator(String password){


        char[] passChar=password.toCharArray();

        byte[] bytes=Passwords.hash(passChar,LoginServlet.SALT,LoginServlet.ITERATIONS );
        String toReturn= Passwords.base64Encode(bytes);


        return toReturn;

    }

private File generateThumbnail(File folder, File originalImageFile) throws IOException {

        final int MAX_W = 400, MAX_H = 400;

        Image thumbnail = null;
        BufferedImage original = null;

        // read original image
        original = ImageIO.read(originalImageFile);

        // get its dimensions
        int w = original.getWidth(null);
        int h = original.getHeight(null);

        // check if we can leave the original dimensions intact
        if (w <= MAX_W & h <= MAX_H) {

            // no resizing
            thumbnail = original;

        } else {

            // one or both dimensions exceed the max allowed size
            float thumbWidth, thumbHeight;
            float scaleFactor = 1.0f;

            // Figure out the scaleFactor - the number by which we should multiply the thumbmail's width and height.
            if (w > h) {
                thumbWidth = MAX_W;
                scaleFactor = thumbWidth / (float) w;
                thumbHeight = (float) h * scaleFactor;
            } else {
                thumbHeight = MAX_H;
                scaleFactor = thumbHeight / (float) h;
                thumbWidth = (float) w * scaleFactor;
            }

            // Actually create the scaled image
            thumbnail = original.getScaledInstance(Math.round(thumbWidth), Math.round(thumbHeight), Image.SCALE_SMOOTH);

        }

        // Write out the thumbnail
        BufferedImage buffer = new BufferedImage(thumbnail.getWidth(null), thumbnail.getHeight(null), original.getType());
        buffer.createGraphics().drawImage(thumbnail, 0, 0, null);
        File thumbnailFile = new File(folder, "thumbnail.png");
        ImageIO.write(buffer, "png", thumbnailFile);
        return thumbnailFile;
    }
}
